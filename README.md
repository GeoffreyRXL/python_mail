# Manage your mail with python3

This script is based on [this article](https://www.thepythoncode.com/article/reading-emails-in-python)
 but I add fews features.

All the following scripts was tested with GMAIL. If you have this kind of error :
```
imaplib.error: b'[AUTHENTICATIONFAILED] Invalid credentials (Failure)'
```
Make sure you allow [less secure apps](https://myaccount.google.com/lesssecureapps?pli=1) on your GMAIL account settings.


## check_or_delete_email_and_store_them.py
Requirements:
```
pip3 install python-dateutil
```

This script allow you to :
* Download your mails and attachments
* Delete mails

And you can choose from which mail sender you want to filter.

Common variables:
```
imap = imaplib.IMAP4_SSL("imap.gmail.com")
username = "<YourEmail>@gmail.com"
password = "<YourPassword>"
# Where do you want to store your mails ?
# Used only with the fetch mail function
mail_path = "/home/<YourUser>/mails/"
```
Each emails are stored in a folder that have the name of the sender.
If the mail have an attachment it will store the mail and the attachment in a folder.

The name of the file that store the email is the concatenation of the date (YearMonthDayHourMinuteSecond) and the subject.

It will give you this result:
```
user@server:~/mails$ tree
.
├── toto@gmail.com
│   ├── 2021-05-02-15-16-04-this_is_a_subject
│   ├── 2021-05-02-15-48-25-NoSubject
│   └── 2021-05-03-10-43-18-wonderfull_subject
│       ├── 2021-05-03-10-43-18-wonderfull_subject
│       ├── First_attachment.txt
│       └── Second_attachment.pdf
└── titi@gmail.com
    ├── 2021-05-02-00-17-09-Hello_there
    └── 2021-05-02-11-12-36-General_Kenobi
```

### Examples :
#### Fetch your mail
If you want to download all your emails from the mailbox INBOX, change the following variables in the script :
```
mailbox = "INBOX"
mailsearch = "ALL"
whitelist=['']
[...]
fetch_mail(imap,whitelist)
#delete_mail(imap,whitelist)
```
If you want to download all your unseen emails from the mailbox INBOX, change the following variables in the script :
```
mailbox = "INBOX"
mailsearch = "UNSEEN"
whitelist=['']
[...]
fetch_mail(imap,whitelist)
#delete_mail(imap,whitelist)
```

If you want to download all the emails in the INBOX from toto@gmail.com :
```
mailbox = "INBOX"
mailsearch = "ALL"
whitelist=['toto@gmail.com']
[...]
fetch_mail(imap,whitelist)
#delete_mail(imap,whitelist)
```
### Delete emails
If you want to delete all the emails in the INBOX:
```
mailbox = "INBOX"
mailsearch = "ALL"
whitelist=['']
[...]
#fetch_mail(imap,whitelist)
delete_mail(imap,whitelist)
```

If you want to delete all the SPAM from toto@gmail.com:
```
mailbox = "SPAM"
mailsearch = "ALL"
whitelist=['toto@gmail.com']
[...]
#fetch_mail(imap,whitelist)
delete_mail(imap,whitelist)
```

## send_mail.py
This script will allow you to:
* Send a mail by receivers
* Send a mail with attachments

A mail is send for each receiver.

Use case : if you have 50 users, it can be painfull to copy paste the email and format it for the python list.
This bash onleliner add your receivers mail list into the send_mail.py script:
```
user@server:~/python_mail$ cat list_receivers.txt
  toto@gmail.com
  titi@gmail.com
  [...]

x='TO = [' && for i in $(cat list_receivers.txt); do x=$x\"$i\"\, ;done && x=$x\] && sed -i "s/^TO\ \=\ \[.*/$x/g" send_mail.py
```

### Examples
If you want to send a mail to toto@gmail.com without attachements
```
TO = ["toto@gmail.com"]
subject = "The email subject"
files_to_send = ['']
[...]
text = """
Hello there,

This is my body mail.

Bye
"""
```


If you want to send a mail to toto@gmail.com with attachements
```
TO = ["toto@gmail.com"]
subject = "The email subject"
files_to_send = ['my_image.png','my_pdf.pdf']
[...]
text = """
Hello there,

This is my body mail.
You will find in the attachment two files.

Bye
"""
```
