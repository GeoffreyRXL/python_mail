import imaplib
import email
from email.header import decode_header
import webbrowser
import os
import re
from dateutil import parser

# Python3 script
# Requierment: pip3 install python-dateutil
# Change your GMAIL settings account :
# Make sure you allow less secure apps on your account settings.


# Two main functions : fetch_mail AND delete_mail
# By default delete_mail is not activate



# create an IMAP4 class with SSL 
imap = imaplib.IMAP4_SSL("imap.gmail.com")

# account credentials
username = "<YourEmail>@gmail.com"
password = "<YourPassword>"

# Where do you want to store your mails ?
# Used only with the fetch mail function
mail_path = "/home/<YourUser>/mails/"

# On which mailbox do you want to check your mail ?
# Example: INBOX, SPAM ...
mailbox = "INBOX"

# What kind of mail ?
# Example: ALL, UNSEEN
mailsearch = "UNSEEN"

# You can choose from which email you want to check
# If you just want to check mail from toto@gmail.com and titi@gmail.com
#whitelist=['toto@gmail.com','titi@gmail.com']
whitelist=['']


def download_body_mail(body,Date,Subject,From,directory):
    # Filename will be : YearMonthDayHourMinuteSecond-Subject 
    # https://pypi.org/project/dateparser/
    longDate = parser.parse(Date)
    YMDdate = (str(re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}',str(longDate)).group(0)).replace(' ','-')).replace(':','-')
    if Subject == '':
        path = directory+"/"+YMDdate+"-NoSubject"
    else:
        path = directory+"/"+YMDdate+"-"+Subject

    # Write the mail in a file
    open(path, "w").write(body)
    print("Mail From: "+From+"   Download in: "+path)


def download_attachments_mail(Date,Subject,From,directory,part):
    # Filename will be : YearMonthDayHourMinuteSecond-Subject 
    # https://pypi.org/project/dateparser/
    longDate = parser.parse(Date)
    YMDdate = (str(re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}',str(longDate)).group(0)).replace(' ','-')).replace(':','-')
    # Create a folder for this mail and move it the mail and the attachments
    if Subject == '':
        path = directory+"/"+YMDdate+"-NoSubject"
        if os.path.isfile(path):
            os.rename(path, path+"_tmp")
            os.makedirs(path)
            os.rename(path+"_tmp", path+"/"+YMDdate+"-NoSubject")
    else:
        path = directory+"/"+YMDdate+"-"+Subject
        if os.path.isfile(path):
            os.rename(path, path+"_tmp")
            os.makedirs(path)
            os.rename(path+"_tmp", path+"/"+YMDdate+"-"+Subject)

    # download attachment and save it
    filepath = os.path.join(path, part.get_filename())
    open(filepath, "wb").write(part.get_payload(decode=True))


####
#Main functions
####

def fetch_mail(imap,whitelist):
    status, messages = imap.select(mailbox)
    status, messages = imap.search(None, mailsearch)
  
    if (status == 'OK') and (messages != [b'']):    
        for num in messages[0].decode("utf-8").split(' '):
            res, msg = imap.fetch(num,'(RFC822)')
            if res == 'OK':
                for response in msg:
                    if isinstance(response, tuple):
                        # parse a bytes email into a message object
                        msg = email.message_from_bytes(response[1])
                        # decode date
                        Date, encoding = decode_header(msg.get("Date"))[0]
                        if isinstance(Date, bytes):
                            Date = Date.decode(encoding)
                        # decode email sender
                        From, encoding = decode_header(msg.get("From"))[0]
                        if isinstance(From, bytes):
                            From = From.decode(encoding)
                        # decode the email subject
                        Subject, encoding = decode_header(msg["Subject"])[0]
                        if isinstance(Subject, bytes):
                            # if it's a bytes, decode to str
                            Subject = Subject.decode(encoding)
                        # create folder for each sender
                        mail_address = re.search('<(.*)>',From).group(1)
                        #print("mail_address:::", mail_address)
                        directory = mail_path + mail_address
                        if (mail_address in whitelist) or (whitelist == ['']):
                            if os.path.exists(directory):
                                pass
                            else:
                                os.makedirs(directory)
                        else:
                            #The adress mail is not in the whitelist
                            continue
                        if not os.path.exists(directory):
                            os.makedirs(directory)
                        # if the email message is multipart
                        if msg.is_multipart():
                            # iterate over email parts
                            for part in msg.walk():
                                # extract content type of email
                                content_type = part.get_content_type()
                                content_disposition = str(part.get("Content-Disposition"))
                                try:
                                    # get the email body
                                    body = part.get_payload(decode=True).decode()
                                except:
                                    pass
                                if content_type == "text/plain" and "attachment" not in content_disposition:
                                    # download mail body
                                    download_body_mail(body,Date,Subject,From,directory)
                                elif "attachment" in content_disposition:
                                    # download attachments
                                    download_attachments_mail(Date,Subject,From,directory,part)
                        else:
                            # extract content type of email
                            content_type = msg.get_content_type()
                            # get the email body
                            body = msg.get_payload(decode=True).decode()
                            if content_type == "text/plain":
                                # print only text email parts
                                download_body_mail(body,Date,Subject,From,directory)
    else:
        print("No new messages")


def delete_mail(imap,whitelist):
    status, messages = imap.select(mailbox)
    status, messages = imap.search(None, mailsearch)
  
    if (status == 'OK') and (messages != [b'']):    
        for num in messages[0].decode("utf-8").split(' '):
            res, msg = imap.fetch(num,'(RFC822)')
            if res == 'OK':
                for response in msg:
                    if isinstance(response, tuple):
                        # parse a bytes email into a message object
                        msg = email.message_from_bytes(response[1])
                        # decode email sender
                        From, encoding = decode_header(msg.get("From"))[0]
                        if isinstance(From, bytes):
                            From = From.decode(encoding)
                        mail_address = re.search('<(.*)>',From).group(1)
                        if mail_address in whitelist:
                            imap.store(num, "+FLAGS", "\\Deleted")
                        else:
                            #The adress mail is not in the whitelist
                            continue
    else:
        print("No mail to delete")



# authenticate
imap.login(username, password)

fetch_mail(imap,whitelist)
#delete_mail(imap,whitelist)
