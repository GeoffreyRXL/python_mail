import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase


# Python3 script
# Change your GMAIL settings account :                          
# Make sure you allow less secure apps on your account settings.


# account credentials
username = "<YourEmail>@gmail.com"
password = "<YourPassword>"
# the receiver's email
TO = ["<receiver1>@gmail.com","<receiver2>@gmail.com"]

# the subject of the email (subject)
subject = "Subject"

#files_to_send = ["a.txt","b.txt"]
files_to_send = ['']


def send_mail(username,password,user,subject,files_to_send):
    # initialize the message we wanna send
    msg = MIMEMultipart("alternative")
    # set the sender's email
    msg["From"] = username
    # set the receiver's email
    msg["To"] = user
    # set the subject
    msg["Subject"] = subject
    # set the body of the email as HTML
    text = """
    Hello,

    This is my body mail.
    
    Bye 
    """
    text_part = MIMEText(text, "plain")
    # attach the email body to the mail message
    # attach the plain text version first
    msg.attach(text_part)
    if files_to_send != ['']:
        for file in files_to_send:
            # open the file as read in bytes
            with open(file, "rb") as f:
                # read the file content
                data = f.read()
                # create the attachment
                attach_part = MIMEBase("application", "octet-stream")
                attach_part.set_payload(data)
            # encode the data to base 64
            encoders.encode_base64(attach_part)
            # add the header
            attach_part.add_header("Content-Disposition", f"attachment; filename= {file}")
            msg.attach(attach_part)

    
    # initialize the SMTP server
    server = smtplib.SMTP("smtp.gmail.com", 587)
    # connect to the SMTP server as TLS mode (secure) and send EHLO
    server.starttls()
    # login to the account using the credentials
    server.login(username, password)
    # send the email
    server.sendmail(username, user, msg.as_string())
    # terminate the SMTP session
    server.quit()


for user in TO:
    send_mail(username,password,user,subject,files_to_send)
